$(document).ready(function () {


    // VARIABLES

    //login variables
    var loginLabelEmail = $('#login__label--email');
    var loginInputEmail = $('#login__input--email');
    var loginLabelPass = $('#login__label--pass');
    var loginInputPass = $('#login__input--pass');
    var loginBtn = $('#login__btn');

    //profile variables    
    var sectionAbout = $('#section__about');
    var sectionSettings = $('#section__settings');
    var aboutBtn = $('#profile__nav__btn--about');
    var settingsBtn = $('#profile__nav__btn--settings');
    var plusBtn = $('.ion-ios-plus');
    var followersCount = $('#followers--count');
    var logoutBtn = $('#logout__btn');

    var profileEdit = $('.profile__main__edit');
    var profileEditBtnCancel = $('#profile__main__btn--cancel');
    var profileEditBtnSave = $('#profile__main__btn--save');
    var profileEditPopup = $('.profile__main__edit__popup');

    var profileEditName = $('#profile__main--name');
    var profileEditWww = $('#profile__main--www');
    var profileEditPhone = $('#profile__main--phone');
    var profileEditLocation = $('#profile__main--location');
    var profileEditText = $('#profile__main__edit--text');
    var profileEditInput = $('#profile__main__edit--input');

    var settingsLabelExistPass = $('#profile__settings__label--existingpass');
    var settingsInputExistPass = $('#profile__settings__input--existingpass');
    var settingsLabelNewPass = $('#profile__settings__label--newpass');
    var settingsInputNewPass = $('#profile__settings__input--newpass');
    var settingsLabelConfirmPass = $('#profile__settings__label--confirmpass');
    var settingsInputConfirmPass = $('#profile__settings__input--confirmpass');
    var settingsBtnSave = $('#profile__settings__btn--save');
    
    // RWD variables
    
    var mobileEdit = $('#edit--mobile--panel');
    var mobileEditSave = $('#edit--mobile--btn--save');
    var mobileEditCancel = $('#edit--mobile--btn--cancel');
    


    // links login - logout
    loginBtn.on('click', function (event) {
        event.preventDefault();
        window.location.href = 'profile.html';
    })
    logoutBtn.on('click', function (event) {
        event.preventDefault();
        window.location.href = 'index.html';
    })


    // Label appear / disappear - animation on input focus or blur
    function LabelVisible() {
        $(this).prev().addClass('animationLabel');
    }

    function LabelInvisible() {
        $(this).prev().removeClass('animationLabel');
    }
    
    var allInputs = $(":input");

    
    allInputs.on('focus', LabelVisible);
    //allInputs.on('blur', LabelInvisible);


    // plus icon - if only getting followers on social media was that easy :P
    plusBtn.on('click', function () {
        var counter = followersCount.html();
        var number = parseInt(counter);
        number++;
        followersCount.html(number);

    })


    // Navigation - About - Settings

    aboutBtn.on('click', function (event) {
        event.preventDefault();
        mobileEdit.css('display', 'none');
        sectionSettings.css('display', 'none');
        sectionAbout.css('display', 'block');

    })

    settingsBtn.on('click', function (event) {
        event.preventDefault();
        sectionAbout.css('display', 'none');
        mobileEdit.css('display', 'none');
        sectionSettings.css('display', 'block');

    })

    // Edit profile - popup appear

    profileEdit.on('click', function () {
        if ($(this).prev().is('#profile__main--name')) {
            profileEditPopup.css('top', '-160px').css('left', '150px');
            profileEditText.text('name');
            profileEditInput.attr('placeholder', profileEditName.text());

        } else if ($(this).prev().is('#profile__main--www')) {
            profileEditPopup.css('top', '-120px').css('left', '160px');
            profileEditText.text('website');
            profileEditInput.attr('placeholder', profileEditWww.text());

        } else if ($(this).prev().is('#profile__main--phone')) {
            profileEditPopup.css('top', '-80px').css('left', '170px');
            profileEditText.text('phone number');
            profileEditInput.attr('placeholder', profileEditPhone.text());

        } else if ($(this).prev().is('#profile__main--location')) {
            profileEditPopup.css('top', '-44px').css('left', '180px');
            profileEditText.text('City, State & ZIP');
            profileEditInput.attr('placeholder', profileEditLocation.text());

        }
        profileEditPopup.css('visibility', 'visible');
        profileEditPopup.show();
    })

    function closePopup() {
        profileEditInput.val(null);
        profileEditPopup.hide();
    };

    profileEditBtnCancel.on('click', closePopup);
    profileEditBtnSave.on('click', closePopup);
    settingsBtnSave.on('click', function (event) {
        event.preventDefault();
    })


    // RWD PROFILE

    
    
    if ($(window).width() < 760) {
        $('.profile__main__edit__popup').remove();
        $('.profile__followers').insertBefore($('.profile__nav--bar'));
        $('.profile__photoframe').insertBefore($('.profile__info'));
        $('.profile__info--name').insertAfter($('.profile__photoframe'));
        $('.profile__reviews--stars').insertAfter($('.profile__info--phone'));
        $('.profile__info--phone').insertBefore($('.profile__info--location'));
        $('.profile__nav--bar').removeClass('profile__nav--bar').addClass('scroll');
        $('<div/>', {
            id: 'profile__info--job',
            class: 'profile__main__div--background',
            text: 'Business'
        }).insertAfter('#profile__main--name');
        $('#profile__edit__btn--mobile').insertAfter('#profile__main__title--id');
        
        profileEdit.on('click', function(event){
        event.preventDefault();
        sectionAbout.css('display', 'none');
        mobileEdit.css('display', 'flex');
        mobileEdit.addClass('edit--mobile');
        
    })
        mobileEditSave.on('click', function(event){
        event.preventDefault();
        mobileEdit.css('display', 'none');
        sectionAbout.css('display', 'initial');
        
    })
        
        mobileEditCancel.on('click', function(event){
        event.preventDefault();
        mobileEdit.css('display', 'none');
        sectionAbout.css('display', 'initial');
        
    })
       
    settingsBtn.on('click', function (event) {
        event.preventDefault();
        sectionAbout.css('display', 'none');
        mobileEdit.css('display', 'none');
        sectionSettings.css('display', 'flex');

    })    
        
    }


});
